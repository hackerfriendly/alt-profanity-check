# Changelog

## Version 1.0.1

* scikit-learn to 1.0.1
* Started canonising code per pylint recommendations: spacing, etc
